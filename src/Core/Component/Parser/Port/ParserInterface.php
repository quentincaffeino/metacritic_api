<?php

declare(strict_types=1);

namespace App\Core\Component\Parser\Port;

use simplehtmldom_1_5\simple_html_dom;

interface ParserInterface
{
    public function getHtmlContent(string $body);

    public function find(simple_html_dom $source, string $selector);
}
