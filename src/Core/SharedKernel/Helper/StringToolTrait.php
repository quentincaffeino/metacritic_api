<?php

declare(strict_types=1);

namespace App\Core\SharedKernel\Helper;

trait StringToolTrait
{
    /**
     * @param string $game_name
     *
     * @return string
     */
    public function format(string $game_name): string
    {
        // convert spaces to -
        $game_name = str_replace(' ', '-', $game_name);
        // Remove &<space>
        $game_name = str_replace('& ', '', $game_name);
        // lowercase
        $game_name = strtolower($game_name);
        // Remove all special chars execept a-z, digits, --sign, ?-sign, !-sign
        $game_name = preg_replace('/[^a-z\d\?!\-]/', '', $game_name);

        return $game_name;
    }
}
