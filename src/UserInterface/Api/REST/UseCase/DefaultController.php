<?php

declare(strict_types=1);

namespace App\UserInterface\Api\REST\UseCase;

use App\Core\Component\Parser\Service\ParserService;
use App\Core\Component\Parser\ValueObject\Selectors;
use App\Core\SharedKernel\Helper\ArrayToolTrait;
use App\Core\SharedKernel\Helper\StringToolTrait;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * @Rest\Route("/rest")
 * Class DefaultController
 */
class DefaultController extends AbstractFOSRestController
{
    use StringToolTrait;
    use ArrayToolTrait;
    /**
     * @var ParserService
     */
    private $parserService;

    /**
     * DefaultController constructor.
     *
     * @param ParserService $parserService
     */
    public function __construct(ParserService $parserService)
    {
        $this->parserService = $parserService;
    }

    /**
     * @Rest\Get("/games/{name}")
     * @QueryParam(name="platform", default="pc", description="Game's platform. PC by default.")
     * @QueryParam(name="selectors", map=true, description="All game's informations that you want to get.", default="all")
     *
     * @param string       $name
     * @param string       $platform
     * @param string|array $selectors
     *
     * @return View
     *
     * @throws \ReflectionException
     */
    public function findByName(string $name, string $platform, $selectors): View
    {
        return View::create(
            $this->parserService->get(
                [
                    'source' => 'metacritic',
                    'params' => [
                        $platform,
                        $this->format($name),
                    ],
                ],
                is_array($selectors) ? $selectors : Selectors::getConstantValues()
            ),
            Response::HTTP_OK
        );
    }

    /**
     * @Rest\Get("/games")
     * @QueryParam(name="title", default="rollcage", description="Game's title to search")
     *
     * @param string $title
     *
     * @return View
     */
    public function searchByName(string $title): View
    {
        $results = $this->parserService->get(
            [
                'source' => 'metacritic_search',
                'params' => [
                    $this->format($title),
                ],
            ],
            [
                'title',
                'platform',
            ]
        );

        $results['result'] = $results['result'] != ParserService::EMPTY_RESULT ? $this->makeAssociativeArrayByKey($results['result'], 'title') : ParserService::EMPTY_RESULT;
        $results['countResult'] = $results['result'] != ParserService::EMPTY_RESULT ? count($results['result']) : 0;

        return View::create(
            $results,
            Response::HTTP_OK
        );
    }
}
